window.addEventListener('load', function() {
    var elements = {
      txtGold: document.querySelector("#res-gold"),
      txtBat: document.querySelector("#res-bat"),
      txtEquipment: document.querySelector("#res-equipment")
    };
    var numGold = 0;
    var maxBat  = 10;
    var powBat  = 0;
    var numChance = 0;
    var equipMod = 1;
    var mine_img = document.getElementById("mine_img");
    var mine_type = "gold";
    var items = [];



    document.querySelector("#gather-resources").addEventListener('click', function(evt) {
      gather();
      powBat = maxBat;
      updateBatText();
      updateGoldText();
      mine_img.src = "mine_on.png";
    });


    document.querySelector("#upgrade-battery").addEventListener('click', function(evt) {
      if(numGold < 5) return;
      maxBat++;
      numGold = numGold - 5;
      updateGoldText();
    });


    document.querySelector("#upgrade-equipment").addEventListener('click', function(evt) {
      if(numGold < 20) return;
      equipMod++;
      numGold = numGold - 20;
      updateGoldText();
      updateEquipmentText();
    });

    function updateGoldText(){
      elements.txtGold.innerHTML = numGold;
    }
    function updateBatText(){
      elements.txtBat.innerHTML = powBat;
    }

    function updateEquipmentText(){
      elements.txtEquipment.innerHTML = equipMod;
    }

    function gather(){

      var findings_list = [];
      // drop rates
      var common = 50.0;
      var uncommon = 10.0;
      var rare = 1.0;
      var mythic = 0.1;

      // drop tables
      if (mine_type === "gold"){
        var common_items = [1,2];
        var uncommon_items = [4,5,6];
        var rare_items = [15,20,25,30];
        var mythic_items = [80,90,100,110,120];
      }


      if (mine_type === "wreckage"){
        var common_items = ["trash"];
        var uncommon_items = ["battery"];
        var rare_items = ["bicycle"];
        var mythic_items = ["equipment"];
      }




      var roll = Math.random()*100;

      while (numChance > 0){
        roll = Math.random()*100;
        if(roll < mythic){
          mythic_item_id = Math.floor(Math.random() * (mythic_items.length));
          result = mythic_items[mythic_item_id];
          if (mine_type === "gold"){
            numGold += result;
          }
          else if(mine_type === "wreckage"){
            items += result;
            if (result === "equipment") {
              equipMod += 5;
              updateEquipmentText()
            }
          }

          findings_list.push(result);
        }
        else if(roll < rare){
          rare_item_id = Math.floor(Math.random() * (rare_items.length));
          result = rare_items[rare_item_id];
          if (mine_type === "gold"){
            numGold += result;
          }
          else if(mine_type === "wreckage"){
            items += result;
          }
          findings_list.push(result);
        }
        else if(roll < uncommon){
          uncommon_item_id = Math.floor(Math.random() * (uncommon_items.length));
          result = uncommon_items[uncommon_item_id];
          if (mine_type === "gold"){
            numGold += result;
          }
          else if(mine_type === "wreckage"){
            items += result;
            if (result==="battery") maxBat += 10;
          }
          findings_list.push(result);
        }
        else if(roll < common){
          common_item_id = Math.floor(Math.random() * (common_items.length));
          result = common_items[common_item_id];
          if (mine_type === "gold"){
            numGold += result;
          }
          else if(mine_type === "wreckage"){
            //items += result;
            // do nothing, don't want to store trash
          }
          findings_list.push(result);
        }
        numChance--;
      }
      for (i = 0; i < findings_list.length; i++){
        if (mine_type === "gold") findings_list[i] += "g";
      }
      document.getElementById('findings-id').innerHTML = findings_list;


    }

    function updateMineType(){
      var radios = document.getElementsByName('mine_type');

      for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
          mine_type = (radios[i].value);
          break;
        }
      }
    }
    function batteryDischarge(){
      updateMineType();
      if (powBat > 0){
        powBat--;
        //numChance++;
        numChance += equipMod;
      }
      else{
        powBat = 0;
        mine_img.src = "mine_off.png"
      }
      updateBatText();
      window.setTimeout(batteryDischarge, 1000);
    }

    batteryDischarge();


});
